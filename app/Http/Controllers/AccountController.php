<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use App\Account;
use App\CreditCard;
use Carbon\Carbon;

class AccountController extends Controller {

    public function validateAge($dateOfBirth){
        $age = Carbon::parse($dateOfBirth)->age;
        if($age > 17 && $age < 66) {
            return true;
        }
        
        return false;
    }

    public function setDateOfBirth($dateOfBirth){
        $setDate = $dateOfBirth;
        if (strpos($dateOfBirth, '/') !== false) {
            $setDate = Carbon::createFromFormat('d/m/Y', $dateOfBirth)->format('Y-m-d');
        }

        return $setDate;
    }

    public function store(){
        $json = file_get_contents(storage_path('challenge.json'));
    	$objs = json_decode($json, true);

        foreach ($objs as $obj) {
            try{
                $account = new Account;
                $creditCard = new CreditCard;

                $account->date_of_birth = $this->setDateOfBirth($obj["date_of_birth"]);

                $age = $this->validateAge($account->date_of_birth);
                if($age) {
                    $account->account = $obj["account"];
                    $account->name = $obj["name"];
                    $account->checked = $obj["checked"];
                    $account->description = $obj["description"];
                    $account->interest = $obj["interest"];
                    $account->email = $obj["email"];
                    $account->creditcard_number = $obj["credit_card"]["number"];

                    $creditCard->number =  $obj["credit_card"]["number"];
                    $creditCard->type = $obj["credit_card"]["type"];
                    $creditCard->name = $obj["credit_card"]["name"];
                    $creditCard->expiration_date =  $obj["credit_card"]["expirationDate"];

                    $account->save();
                    $creditCard->save();
                }
            }
            catch (QueryException $e){
                $errorCode = $e->errorInfo[1];
                if($errorCode == 1062){
                    error_log("Duplicate Entry");
                }
            }
        }
        echo "succes";
    }
}
