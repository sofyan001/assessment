<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CreditCard extends Model{
    protected $table = 'creditcard';
    public $timestamps = false;

    protected $fillable = [
        'number', 'type', 'name', 'expiration_date'
    ];
}
