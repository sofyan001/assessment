<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model{
    protected $table = 'User';
    public $timestamps = false;

    protected $fillable = [
        'account', 'name', 'checked', 'description', 'interest', 'date_of_birth', 'email', 'creditcard_number'
    ];
}
